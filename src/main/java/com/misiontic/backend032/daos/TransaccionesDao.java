package com.misiontic.backend032.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.misiontic.backend032.modelos.Transacciones;

public interface TransaccionesDao extends JpaRepository<Transacciones, Long> {
    
}
