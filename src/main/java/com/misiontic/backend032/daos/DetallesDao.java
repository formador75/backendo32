package com.misiontic.backend032.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.misiontic.backend032.modelos.Detalles;

public interface DetallesDao extends JpaRepository<Detalles, Long> {

}
