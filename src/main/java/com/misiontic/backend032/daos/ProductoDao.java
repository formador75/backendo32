package com.misiontic.backend032.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.misiontic.backend032.modelos.Productos;

public interface ProductoDao extends JpaRepository<Productos, Long>{


}