package com.misiontic.backend032.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transacciones")
public class Transacciones {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     private long idTransaccion;

     @Column(name="tipotransaccion", nullable = false)
     private String tipoTransaccion;

     @Column(name="fechatransaccion", nullable = false)
     private String fechaTransaccion;

     @Column(name="vendedor", nullable = false)
     private String vendedor;

     @Column(name="comprador", nullable = false)
     private String comprador;

     @Column(name="total", nullable = false)
     private double total;

    public Transacciones() {
    }

    public Transacciones(String tipoTransaccion, String fechaTransaccion, String vendedor, String comprador,
            double total) {
        this.tipoTransaccion = tipoTransaccion;
        this.fechaTransaccion = fechaTransaccion;
        this.vendedor = vendedor;
        this.comprador = comprador;
        this.total = total;
    }

    public long getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(long idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(String fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Transacciones [comprador=" + comprador + ", fechaTransaccion=" + fechaTransaccion + ", idTransaccion="
                + idTransaccion + ", tipoTransaccion=" + tipoTransaccion + ", total=" + total + ", vendedor=" + vendedor
                + "]";
    }


     

}
