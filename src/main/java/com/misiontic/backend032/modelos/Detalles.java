package com.misiontic.backend032.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="detalles")
public class Detalles {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idDetalle;

    @Column(name="valordetalle", nullable = false)
    private double valorDetalle;

    @Column(name="cantidaddetalle", nullable = false)
    private double cantidadDetalle;

    @Column(name="totaldetalle", nullable = false)
    private double totalDetalle;

    @ManyToOne
    @JoinColumn(name="idProducto")
    private Productos producto;

    @ManyToOne
    @JoinColumn(name="idTransaccion")
    private Transacciones transaccion;

    public Detalles() {
    }

    public Detalles(double valorDetalle, double cantidadDetalle, double totalDetalle) {
        this.valorDetalle = valorDetalle;
        this.cantidadDetalle = cantidadDetalle;
        this.totalDetalle = totalDetalle;
    }

    public long getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(long idDetalle) {
        this.idDetalle = idDetalle;
    }

    public double getValorDetalle() {
        return valorDetalle;
    }

    public void setValorDetalle(double valorDetalle) {
        this.valorDetalle = valorDetalle;
    }

    public double getCantidadDetalle() {
        return cantidadDetalle;
    }

    public void setCantidadDetalle(double cantidadDetalle) {
        this.cantidadDetalle = cantidadDetalle;
    }

    public double getTotalDetalle() {
        return totalDetalle;
    }

    public void setTotalDetalle(double totalDetalle) {
        this.totalDetalle = totalDetalle;
    }

    public Productos getProducto() {
        return producto;
    }

    public void setProducto(Productos producto) {
        this.producto = producto;
    }

    public Transacciones getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Transacciones transaccion) {
        this.transaccion = transaccion;
    }

    

}
