package com.misiontic.backend032.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="productos")
public class Productos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idProducto;

    @Column(name="nombreproducto", nullable = false)
    private String nombreProducto;
    
    @Column(name="preciocompra", nullable = false)
    private double precioCompra;

    @Column(name="precioventa", nullable = false)
    private double precioVenta;

    @Column(name="cantidad", nullable = false)
    private double cantidad;

    public Productos() {
    }

    public Productos(String nombreProducto, double precioCompra, double precioVenta, double cantidad) {
        this.nombreProducto = nombreProducto;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
        this.cantidad = cantidad;
    }

    public long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(long idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public double getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(double precioCompra) {
        this.precioCompra = precioCompra;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "Productos [cantidad=" + cantidad + ", idProducto=" + idProducto + ", nombreProducto=" + nombreProducto
                + ", precioCompra=" + precioCompra + ", precioVenta=" + precioVenta + "]";
    }
    
    
    

}
