package com.misiontic.backend032.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.backend032.daos.TransaccionesDao;
import com.misiontic.backend032.modelos.Transacciones;

@RestController
@RequestMapping("/api")
public class ControladorTransacciones {
    
    @Autowired
    TransaccionesDao transaccionDao;

    @PostMapping("/transacciones")
    public ResponseEntity<Transacciones> guardarTransaccion(@RequestBody Transacciones transaccion){
        Transacciones nuevoTransaccion = new Transacciones(transaccion.getTipoTransaccion(), transaccion.getFechaTransaccion(), transaccion.getVendedor(), transaccion.getComprador(), transaccion.getTotal());
        return new ResponseEntity<>(transaccionDao.save(nuevoTransaccion), HttpStatus.CREATED);
    }

    @PutMapping("/transacciones/{id}")
    public ResponseEntity<Transacciones> actualizarTransaccion(@PathVariable long id, @RequestBody Transacciones transaccion){
        Optional<Transacciones> _transaccion =  transaccionDao.findById(id);
        if(_transaccion.isPresent()){
            Transacciones transaccionActualizar = _transaccion.get();
            transaccionActualizar.setFechaTransaccion(transaccion.getFechaTransaccion());
            transaccionActualizar.setTipoTransaccion(transaccion.getTipoTransaccion());
            transaccionActualizar.setComprador(transaccion.getComprador());
            transaccionActualizar.setVendedor(transaccion.getVendedor());
            transaccionActualizar.setTotal(transaccion.getTotal());

         return new ResponseEntity<>(transaccionDao.save(transaccionActualizar), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/transacciones")
    public ResponseEntity<List<Transacciones>> listarTransacciones(){


        List<Transacciones> transacciones = new ArrayList<Transacciones>();

       transaccionDao.findAll().forEach(transacciones::add);
      
       if (transacciones.isEmpty()){

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
       }else{

        return new ResponseEntity<>(transacciones, HttpStatus.OK);
       }
 
        }


    @GetMapping("/transacciones/{id}")
    public ResponseEntity<Transacciones> verTransaccion(@PathVariable Long id){

        Optional<Transacciones> transaccion =  transaccionDao.findById(id);
        if(transaccion.isPresent()){
            return new ResponseEntity<>(transaccion.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(new Transacciones(),HttpStatus.OK);
        }
 
        }


    @DeleteMapping("/transacciones/{id}")
    public ResponseEntity<HttpStatus> borrarTransaccion(@PathVariable Long id){

        transaccionDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }



}
