package com.misiontic.backend032.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.ws.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.backend032.daos.ProductoDao;
import com.misiontic.backend032.modelos.Productos;

@CrossOrigin("*") 
@RestController
@RequestMapping("/api")
public class ControladorProducto {
    
    @Autowired
    ProductoDao productoDao;

    @PostMapping("/productos")
    public ResponseEntity<Productos> guardarProducto(@RequestBody Productos producto){
        Productos nuevoProducto = new Productos(producto.getNombreProducto(), producto.getPrecioCompra(), producto.getPrecioVenta(), producto.getCantidad());
        return new ResponseEntity<>(productoDao.save(nuevoProducto), HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<Productos> actualizarProducto(@PathVariable long id, @RequestBody Productos producto){
        Optional<Productos> _producto =  productoDao.findById(id);
        if(_producto.isPresent()){
            Productos productoActualizar = _producto.get();
            productoActualizar.setNombreProducto(producto.getNombreProducto());
            productoActualizar.setPrecioCompra(producto.getPrecioCompra());
            productoActualizar.setPrecioVenta(producto.getPrecioVenta());
            productoActualizar.setCantidad(producto.getCantidad());
         return new ResponseEntity<>(productoDao.save(productoActualizar), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/productos")
    public ResponseEntity<List<Productos>> listarProductos(){


        List<Productos> productos = new ArrayList<Productos>();

       productoDao.findAll().forEach(productos::add);
      
       if (productos.isEmpty()){

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
       }else{

        return new ResponseEntity<>(productos, HttpStatus.OK);
       }
 
        }


    @GetMapping("/productos/{id}")
    public ResponseEntity<Productos> verProducto(@PathVariable Long id){

        Optional<Productos> producto =  productoDao.findById(id);
        if(producto.isPresent()){
            return new ResponseEntity<>(producto.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(new Productos(),HttpStatus.OK);
        }
 
        }


    @DeleteMapping("/productos/{id}")
    public ResponseEntity<HttpStatus> borrarProducto(@PathVariable Long id){

        productoDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }



}
